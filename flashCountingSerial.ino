/*
    Copyright (C) 2018  Gabriel Siano
    Universidad Nacional del Litoral/CONICET
    <gabrielsiano_at_gmail_dot_com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const byte pulsePin = 2;
volatile boolean pulsePinState = false;
int counter = 0;
String str;
unsigned long tiempos[1400]; //suggested upper limit for Arduino Mega (150 for Arduino UNO)
int flashPerSP = 33;//always equal or less than tiempos (150 for Arduino UNO)
//NOTE1: flashPerSP indicates the number of flashes of the excitation lamp for each emission spectrum (i.e. the number of trigger pulses).
//This depends on the number of variables to be read in each emission spectrum (nVarsEm), and also on the number of times that each variable is read (nRep).
//By default, it is assumed that each variable requires a single flash (nRep=1).
//In the fluorimeter GUI, when going from maximum speed to minimum speed (nm / min), nRep grows (1-2-4-8 ..)
//During the registering of a spectrum, the fluorimeter flashes every 12.5 mS (80 Hz).
//When the monochromators are restarted (0.5-1 second), the accumulated time values in "tiempos" are sent through the serial port to a computer.

//NOTE2: This sketch uses "micros". From Arduino reference (https://www.arduino.cc/reference/en/language/functions/time/micros/) 
//Returns the number of microseconds since the Arduino board began running the current program. This number will overflow (go back to zero),
//after approximately 70 minutes. On 16 MHz Arduino boards (e.g. Duemilanove and Nano), this function has a resolution of four microseconds 
//(i.e. the value returned is always a multiple of four).


void setup() {
  pinMode(pulsePin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pulsePin), setflag, RISING);
  Serial.begin(230400);
  delay(1000);
  Serial.print("Iniciado (flashPerSP=");
  Serial.print(flashPerSP);
  Serial.println(")");
}//setup



void loop() {
  if (pulsePinState) {
    tiempos[counter] = micros();
    pulsePinState = false;
    counter = counter + 1;
  }//if pulsePinState

  if (counter == flashPerSP) {
    counter = 0;
    for (int i = 0; i < flashPerSP; i = i + 1) {
      Serial.println(tiempos[i]);
    }
  }//counter == flashPerSP

  if (Serial.available() > 0) {
    str = Serial.readStringUntil('\n');
    str.trim();
    if (str.startsWith("FPS")) {//FPS: Flash Per Spectrum
      flashPerSP = (str.substring(3)).toInt();
      Serial.print("Nuevo FlashPerSpectrum: ");
      Serial.println(flashPerSP);
    }//if (str.startsWith("FPS"))

    else if (str.startsWith("RCT")) {//RCT: Reset Counter
      counter = 0;
      Serial.println("counter=0");
    }//if (str.startsWith("RCT"))
    else if (str.startsWith("CTis")) {//CTis: Counter is?
      Serial.print("counter is: ");
      Serial.println(counter);
    }//if (str.startsWith("CTis")
  }//if (Serial.available() > 0)
}//loop


void setflag() {
  pulsePinState = true;
}